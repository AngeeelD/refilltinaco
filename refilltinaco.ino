/*Proyecto para el llenado automático de un tinaco por medio de Arduino*/
/*El arduino evalua la cantidad de agua que existe en un contenedor y 
la compara con la cantidad de agua faltante en el tinaco y acciona la
bomba de agua en momentos en que se requiere agua para que siempre este lleno*/

//MATERIALES
//ARDUINO MEGA2560
//RELAY 
//DISPLAY LCD FOR ARDUINO 16x2
//POTENCIOMETRO DE 10K
//SENSORES CAPACITIVOS HECHOS A MANO 

/*~~~~~~~~~~~~~~~~~~~~~~~~~LIBRERIAS~~~~~~~~~~~~~~~~~~~~~~~~~*/
#include "lecturaCapacitiva.ino"
#include <LiquidCrystal.h>

/*~~~~~~~~~~~~~~~~~~~~~~~~~VARIABLES~~~~~~~~~~~~~~~~~~~~~~~~~*/
lecturaCapacitiva lectorCapacitivo;
//39-44 PINES DEL ARDUINO
LiquidCrystal lcd(39, 40, 41, 42, 43, 44);
//CONEXION DE LA PANTALLA LCD DE IZQUIERDA A DERECHA
/*
	  1	 2  3  4  5  6  7  8  9  10 11 12 13 14 15 16
	__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|_________________________
	|																		|
	|	1.VSS-GND 		5.RW-GND		9.D2-nada		13.D6-pin43			|
	|	2.VDD-5v		6.E-pin40		10.D3-nada		14.D7-pin44			|
	|	3.PS-pot		7.D0-nada		11.D4-pin41		15.A-10k			|
	|	4.RW-pin39		8.D1-nada		12.D5-pin42		16.K-GND			|
	|_______________________________________________________________________|

*/

/*RELAY SRD-05VCD-SLC*/
/*
		CORRIENTE	  EXTREMO
		   VCA		   BOMBA
			|____   _____|
				|	|
				|	|
	_______NC__CMN__NA___
	|		X	X	X	|
	|					|
	|					|
	|					|
	|					|
	|					|
	|					|
	|					|
	|___________________|
			|	|	|	
		   pin VCC GND
		   47
NOTA: Mantengo un led al pin 47 para saber el estado de la bomba de manera mas visual.


*/

/*SWITCH ACCIONADO MANUAL DE BOMBA*/
/*
		0		1
				/ \
	___________/___\__
	|				|
	|				|
	|				|
	|_______________|
		|	|	|
		|	|	|
	   VCC pin  na
			48  da
*/

/*POTENCIOMETRO 10k*/
/*
		 _____
		 | 	 |
		 | 	 |
		 | 	 |
		 | 	 |
  _______|___|_______
  |		    		|
  |_________________|
      |    |    |
      |    |    |
     VCC  pin  GND
           3
          lcd
*/

/*SENSORES DE AGUA */
//La enumeración va de [0]=>BAJO hasta [6]=>ALTO
//22-28 PINES DEL ARDUINO
int sensoresTinaco[7] = {22, 23, 24, 25, 26, 27, 28}; //EL POLO COMÚN VA A NADA (Queda volando); Es el polo que va hasta abajo del contenedor
int sensoresContenedor[6] = {31, 32, 33, 34, 35, 36}; //EL POLO COMÚN VA A NADA (Queda volando); Es el polo que va hasta abajo del contenedor
int noSensoresTinaco = 0;
int noSensoresContenedor = 0;
int sensorApagarBomba = 3;
int sensorEncenderBomba = 4;

/*PIN ACTUADOR DE BOMBA*/
//PIN ACCIONADOR, ESTE VA A UN LED Y A SU VEZ A UN PIN DEL RELAY
int bomba = 47;
boolean usandoBomba = false;

//VARIABLE INDICADORA DEL NIVEL DEL AGUA
int nivelAguaTinaco = -1;
int nivelAguaContenedor = -1;

//SENSIBILIDAD DE SENSORES
int SENSIBILIDAD = 15;

/*INTERRUPTOR MANUAL DE LA BOMBA*/
int interruptorBomba = 48;



/*~~~~~~~~~~~~~~~~~~~~~~~~~PROGRAMA~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/*METODOS ESENCIALES PARA EL FUNCIONAMIENTO DEL PROGRAMA*/
//METODO CONFIGURACION
void setup() {
	noSensoresTinaco = sizeof(sensoresTinaco) / sizeof(int);
	noSensoresContenedor = sizeof(sensoresContenedor) / sizeof(int);

	//INICIALIZACION SENSORES DEL TINACO
	for(int i = 0; i < noSensoresTinaco ; i++){
		pinMode(sensoresTinaco[i], INPUT);
	}

	//INICIALIZACION DE SENSORES DEL CONTENEDOR DE AGUA (SUMINISTRO)
	for(int i = 0; i < noSensoresContenedor ; i++){
		pinMode(sensoresContenedor[i], INPUT);
	}

	//BOMBA EN MODO SALIDA
	pinMode(bomba, OUTPUT);

	//INTERRUPTOR EN MODO ENTRADA
	pinMode(interruptorBomba, INPUT);

	lectorCapacitivo = lecturaCapacitiva();

	Serial.begin(9600);
	Serial.println("Comunicacion serial Activada");

	//INICIALIZACION PANTALLA LCD
	lcd.begin(16, 2);
	//lcd.print("hello, world!");
}
//METODO DE BUCLE INFINITO
void loop() {
	rutinaTinaco();
	rutinaPila();
	if(!leerInterruptor()){
		if(usandoBomba){
			delay(800);
		}
		rutinaBomba();
		if(usandoBomba){
			delay(800);
		}
	}else{
		encenderBomba();
	}
	delay(500);
}

//RUTINAS PRINCIPALES DEL PROGRAMA
boolean leerInterruptor(){
	if(lectorCapacitivo.obtenerLecturaCapacitiva(interruptorBomba) == 0){
		return true;
	}else{
		return false;
	}
}
void rutinaPila(){
	String estadoSensores = addJsonObject("PILA", obtenerEstadoSensoresContenedor(sensoresContenedor, (noSensoresContenedor)));
	Serial.println(estadoSensores);
	Serial.print("NIVEL DEL AGUA:\n\t");
	String mensaje = String("PILA       ");
	if(verificarEstadoSensores(sensoresContenedor, nivelAguaContenedor)){
		if( (getNivelContenedor()) == noSensoresContenedor ){
			mensaje.concat(" ");
			mensaje.concat(100);
			mensaje.concat("%");
		}else{
			if(nivelAguaContenedor == -1){
				mensaje.concat("VACIO");
			}else{
				mensaje.concat("  ");
				mensaje.concat((getNivelContenedor())*(100/noSensoresContenedor));		
				mensaje.concat("%");
			}
		}
		//if(!usandoBomba){
			limpiarLinea2();
			imprimirLCD(1, mensaje);	
		//}
		Serial.println(nivelAguaContenedor);
	}else{
		if(nivelAguaContenedor == -1){
			mensaje.concat("VACIA");
		}
		Serial.print("EXISTE UNA FALLA EN EL SENSOR #");
		Serial.println(BuscarSensorConFalla(sensoresContenedor, nivelAguaContenedor));
	}
}
void rutinaTinaco(){
	String estadoSensores = addJsonObject("TINACO", obtenerEstadoSensoresTinaco(sensoresTinaco, (noSensoresTinaco)));
	Serial.println(estadoSensores);
	Serial.print("NIVEL DEL AGUA:\n\t");
	String mensaje = String("TINACO     ");
	if(verificarEstadoSensores(sensoresTinaco, nivelAguaTinaco)){
		if( (getNivelTinaco()) == noSensoresTinaco ){
			mensaje.concat(" ");
			mensaje.concat(100);
			mensaje.concat("%");
		}else{
			if(nivelAguaTinaco == -1){
				mensaje.concat("VACIO");
			}else{
				mensaje.concat("  ");
				mensaje.concat((getNivelTinaco())*(100/noSensoresTinaco));		
				mensaje.concat("%");
			}
		}
		//if(!usandoBomba){
			limpiarLinea1();
			imprimirLCD(0, mensaje);	
		//}
		Serial.println(nivelAguaTinaco);	
	}else{
		Serial.print("EXISTE UNA FALLA EN EL SENSOR #");
		Serial.println(BuscarSensorConFalla(sensoresTinaco, nivelAguaTinaco));
	}
}
void rutinaBomba(){
	if( (getNivelContenedor()) <= noSensoresContenedor 
		&& (getNivelContenedor()) >= sensorEncenderBomba
		&& (getNivelTinaco()) < noSensoresTinaco){
		encenderBomba();
		limpiarPantalla();
		imprimirLCD(0, "    LLENANDO    ");
		imprimirLCD(1, "     TINACO     ");
		usandoBomba = true;
	}else if((getNivelContenedor()) < sensorApagarBomba 
		|| (getNivelTinaco()) == noSensoresTinaco){
		usandoBomba = false;
		apagarBomba();
	}
}

/*FUNCIONES QUE DAN ESENCIA AL PROGRAMA*/
//OPERACIONES PRINCIPALES
String obtenerEstadoSensoresTinaco(int sensores[], int limite){
	nivelAguaTinaco = -1;
	String respuesta = String("");
	for(int i = 0 ; i < limite ; i++){
		String sensor = String("Sensor #");
		sensor.concat(i);
		int sensorValue = lectorCapacitivo.obtenerLecturaCapacitiva(sensores[i]);
		respuesta.concat(addJsonObject( sensor, sensorValue ));
		if( i < limite-1 ){
			respuesta.concat(",");
		}
		//capturar nivel de agua
		if(sensorValue > SENSIBILIDAD){
			//IMPRIMIR NIVELES
			nivelAguaTinaco = i;
		}
	}
	return respuesta;
}
String obtenerEstadoSensoresContenedor(int sensores[], int limite){
	nivelAguaContenedor = -1;
	String respuesta = String("");
	for(int i = 0 ; i < limite ; i++){
		String sensor = String("Sensor #");
		sensor.concat(i);
		int sensorValue = lectorCapacitivo.obtenerLecturaCapacitiva(sensores[i]);
		respuesta.concat(addJsonObject( sensor, sensorValue ));
		if( i < limite-1 ){
			respuesta.concat(",");
		}
		//capturar nivel de agua
		if(sensorValue > SENSIBILIDAD){
			//IMPRIMIR NIVELES
			nivelAguaContenedor = i;
		}
	}
	return respuesta;
}
boolean verificarEstadoSensores(int sensores[], int nivelActual){
	if(nivelActual >= 0){
		int valorSensor = lectorCapacitivo.obtenerLecturaCapacitiva(sensores[nivelActual]);
		if(valorSensor > SENSIBILIDAD ){
			verificarEstadoSensores(sensores, nivelActual-1);
		}else{
			return false;
		}
	}else{
		return true;
	}
}
int BuscarSensorConFalla(int sensores[], int nivelActual){
	if(nivelActual >= 0){
		if(lectorCapacitivo.obtenerLecturaCapacitiva(sensores[nivelActual] > SENSIBILIDAD) ){
			verificarEstadoSensores(sensores, nivelActual-1);
		}else{
			return nivelActual;
		}
	}else{
		return -1;
	}
}
void encenderBomba(){
	digitalWrite(bomba, HIGH);
}
void apagarBomba(){
	digitalWrite(bomba, LOW);
}
int getNivelContenedor(){
	return (nivelAguaContenedor+1);	
}
int getNivelTinaco(){
	return (nivelAguaTinaco+1);
}

/*METODOS DE UTILERIA SECUNDARIA*/
//TRATAMIENTO DE CADENAS Y CREACION DE OBJECTOS JSON
String addJsonObject( String nombreObjecto, int value ) {
	return "{" + nombreObjecto + ":" + value + "}" ;
}
String addJsonObject( String nombreObjecto, String value ) {
	return "{" + nombreObjecto + ":" + value + "}" ;
}

/*METODOS PARA EL MANEJO DE IMPRESION DE CADENAS SOBRE EL DISPLAY LCD 16x2*/
void imprimirLCD(int linea, String mensaje){
	lcd.setCursor(0, linea);
	lcd.print(mensaje);
}
void limpiarLinea1(){
	lcd.setCursor(0, 0);
	lcd.print("               ");
}
void limpiarLinea2(){
	lcd.setCursor(0, 1);
	lcd.print("               ");
}
void limpiarPantalla(){
	lcd.setCursor(0, 0);
	lcd.print("               ");
	lcd.setCursor(0, 1);
	lcd.print("               ");
}


